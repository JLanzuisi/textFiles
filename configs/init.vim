set nocompatible
filetype on
set clipboard+=unnamedplus
set nohlsearch
set termguicolors
map <leader>s :setlocal spell! spelllang=es<CR>

let g:python3_host_prog = expand('~\AppData\Local\Programs\Python\Python38\python.exe')

" Vertically center document when entering insert mode
autocmd InsertEnter * norm zz

" Shortcutting split navigation
"map <C-h> <C-w>h
"map <C-j> <C-w>j
"map <C-k> <C-w>k
"map <C-l> <C-w>l

inoremap <silent>  <S-Insert>  <C-R>+

call plug#begin('~/AppData/Local/nvim/plugged')

Plug 'lervag/vimtex'
Plug 'jhradilek/vim-docbk'
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'sirVer/ultisnips'
Plug 'honza/vim-snippets'
Plug 'vim-airline/vim-airline'
Plug 'jiangmiao/auto-pairs'
Plug 'tpope/vim-vinegar'
Plug 'unblevable/quick-scope'
Plug 'morhetz/gruvbox'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-commentary'

call plug#end()

call deoplete#custom#var('omni', 'input_patterns', {
      \ 'tex': g:vimtex#re#deoplete
      \})

call deoplete#custom#option('ignore_sources', {'_': ['around', 'buffer']})
call deoplete#custom#source('_', 'max_menu_width', 80)

" Quickscope
let g:qs_highlight_on_keys = ['f', 'F', 't', 'T']
highlight QuickScopePrimary guifg='#afff5f' gui=underline ctermfg=155 cterm=underline
highlight QuickScopeSecondary guifg='#5fffff' gui=underline ctermfg=81 cterm=underline

set conceallevel=1
let g:tex_flavor  = 'latex'
let g:tex_conceal = 'abdmg'
let g:vimtex_fold_manual = 1
let g:vimtex_latexmk_continuous = 1
let g:vimtex_compiler_progname = 'C:\Users\Jhonny\bin\Neovim\bin'
let g:vimtex_view_general_viewer = 'okular'
let g:vimtex_toc_config = {
    \'split_pos'   : ':vert :botright',
    \'split_width':  50,
    \}

augroup filetypedetect
	" Some LaTeX types
	au! BufRead,BufNewFile *.cls setfiletype tex
	au! BufRead,BufNewFile *.lco setfiletype tex
augroup END

let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<s-tab>"
let g:UltiSnipsListSnippets="<c-tab>"
set rtp+=~/uni/

let g:deoplete#enable_at_startup = 1

set bg=light
let g:gruvbox_contrast_light = 'hard'
colorscheme gruvbox
let g:airline_theme='gruvbox'

let g:netrw_banner = 0
let g:netrw_liststyle = 2
let g:netrw_browse_split = 3
let g:netrw_winsize = 20
augroup ProjectDrawer
  autocmd!
  autocmd VimEnter * :Vexplore
  autocmd VimEnter * wincmd l
augroup END

"augroup CleanAndcdonTeXFiles
	"autocmd!
	"autocmd VimEnter *.tex silent! lcd ~/latex/
	"autocmd VimLeave *.tex !latexmk -c
"augroup END
